package com.test.utils

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.test.musicfestival.data.model.RecordLabel
import com.test.musicfestival.util.ErrorType
import com.test.musicfestival.util.Resource

class TestDataGenerator {

    private val jsonData = "[{\"recordLabel\":\"Outerscope\",\"musicBands\":[{\"name\":\"Squint-281\",\"festivals\":[{\"name\":\"Small Night In\"}]},{\"name\":\"Summon\",\"festivals\":[{\"name\":\"Twisted Tour\"}]}]},{\"recordLabel\":\"Fourth Woman Records\",\"musicBands\":[{\"name\":\"The Black Dashes\",\"festivals\":[{\"name\":\"Small Night In\"}]},{\"name\":\"Jill Black\",\"festivals\":[{\"name\":\"LOL-palooza\"}]}]}]"
    private var festivals: List<RecordLabel>
    init {
        val typeToken = object : TypeToken<List<RecordLabel>>() {}.type
        festivals = Gson().fromJson(jsonData, typeToken)
    }

    fun generateFestivals(): List<RecordLabel>{
        return festivals
    }

    fun generateEmptyFestivalList(): List<RecordLabel>{
        return emptyList()
    }
    fun generateEmptyDataError(): Resource<List<RecordLabel>> {
        return Resource.Error(ErrorType.EMPTY_DATA)
    }

    fun generateThrottlingError(): Resource<List<RecordLabel>> {
        return Resource.Error(ErrorType.THROTTLING)
    }
}